

function HyperAbstract() {
	var all_diseases;
	$.ajax({
	url: "http://appserver.alunos.di.fc.ul.pt/~aw004/rest/suggestions?q=%&t=4000",
	type: "GET",
	dataType: "json",
	success: function(data) {
			// ir buscar todas as doenças e ordenar por length DESC            
			all_diseases = data.sort(function(a,b){return b.length - a.length});
			// console.log(all_diseases);
 
			
			// Selecionar artigos, ... 
			// var thisPage = $(".articletitle,.articlesummary,.articleabstract");
			var thisPage = $(".articleabstract");

			// para ver o texto
			// console.log(thisPage.html());
			//Traduzir para códigos as doenças que aparecem na página
			var translator = [];
			var increment = 1000;
			var cod;
			var codigos = [];
			all_diseases.forEach(function(dis){
				
				if(thisPage.html().toLowerCase().indexOf(dis) > -1){
					cod = '%_!'+increment.toString();
					increment = increment + 1;
					translator.push({code: cod, disease: dis});
					// console.log(translator);
					re = new RegExp(dis, "ig");
					thisPage.html(thisPage.html().replace(re, cod));
				}
			})
				translator.forEach(function(t){
					if(thisPage.html().toLowerCase().indexOf(t["code"]) > -1){
						dis = t["disease"];
						str_str = "'"+ escape(t["disease"])+"'";
						var repl = new RegExp(t["code"], "ig");
						thisPage.html(thisPage.html().replace(repl, '<a href="javascript:refreshText('+str_str+');">'+dis+'</a>'));
						// console.log(thisPage.html());
					}
			})
	},
	error: function() { alert("Error on obtaining the highlights");}
	});
}


function HyperRelatedDis() {
	var all_diseases;
	$.ajax({
	url: "http://appserver.alunos.di.fc.ul.pt/~aw004/rest/suggestions?q=%&t=4000",
	type: "GET",
	dataType: "json",
	success: function(data) {
			// ir buscar todas as doenças e ordenar por length DESC            
			all_diseases = data.sort(function(a,b){return b.length - a.length});
			// console.log(all_diseases);
 
			
			// Selecionar artigos, ... 
			// var thisPage = $(".articletitle,.articlesummary,.articleabstract");
			var thisPage = $(".articlerelateddis");

			// para ver o texto
			// console.log(thisPage.html());
			//Traduzir para códigos as doenças que aparecem na página
			var translator = [];
			var increment = 1000;
			var cod;
			var codigos = [];
			all_diseases.forEach(function(dis){
				
				if(thisPage.html().toLowerCase().indexOf(dis) > -1){
					cod = '%_!'+increment.toString();
					increment = increment + 1;
					translator.push({code: cod, disease: dis});
					// console.log(translator);
					re = new RegExp(dis, "ig");
					thisPage.html(thisPage.html().replace(re, cod));
				}
			})
				translator.forEach(function(t){
					if(thisPage.html().toLowerCase().indexOf(t["code"]) > -1){
						dis = t["disease"];
						str_str = "'"+ escape(t["disease"])+"'";
						var repl = new RegExp(t["code"], "ig");
						thisPage.html(thisPage.html().replace(repl, '<a href="javascript:refreshText('+str_str+');">'+dis+'</a>'));
						// console.log(thisPage.html());
					}
			})
	},
	error: function() { alert("Error on obtaining the highlights");}
	});
}




function HyperMetaDis() {
	var all_diseases;
	$.ajax({
	url: "http://appserver.alunos.di.fc.ul.pt/~aw004/rest/suggestions?q=%&t=4000",
	type: "GET",
	dataType: "json",
	success: function(data) {
			// ir buscar todas as doenças e ordenar por length DESC            
			all_diseases = data.sort(function(a,b){return b.length - a.length});
			// console.log(all_diseases);
 
			
			// Selecionar artigos, ... 
			// var thisPage = $(".articletitle,.articlesummary,.articleabstract");
			var thisPage = $(".relateddiseases");

			// para ver o texto
			// console.log(thisPage.html());
			//Traduzir para códigos as doenças que aparecem na página
			var translator = [];
			var increment = 1000;
			var cod;
			var codigos = [];
			all_diseases.forEach(function(dis){
				
				if(thisPage.html().toLowerCase().indexOf(dis) > -1){
					cod = '%_!'+increment.toString();
					increment = increment + 1;
					translator.push({code: cod, disease: dis});
					// console.log(translator);
					re = new RegExp(dis, "ig");
					thisPage.html(thisPage.html().replace(re, cod));
				}
			})
				translator.forEach(function(t){
					if(thisPage.html().toLowerCase().indexOf(t["code"]) > -1){
						dis = t["disease"];
						str_str = "'"+ escape(t["disease"])+"'";
						var repl = new RegExp(t["code"], "ig");
						thisPage.html(thisPage.html().replace(repl, '<a href="javascript:refreshText('+str_str+');">'+dis+'</a>'));
						// console.log(thisPage.html());
					}
			})
	},
	error: function() { alert("Error on obtaining the highlights");}
	});
}

// HyperAbstract();
// HyperRelatedDis();
// HyperMetaDis();
