<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once 'database.php';
include_once 'object_disease.php';
include_once 'SimpleRest.php';



class Read extends SimpleRest {

    function readAll() {

      // instantiate database and disease object
      $database = new Database();
      $db = $database->getConnection();

      // initialize object
      $disease = new Disease($db);
      // query disease
	  $stmt = $disease->read();


      //$num = $stmt->rowCount();
      if (empty($stmt)) {
          $statusCode= 404;
          $stmt = array('error' => 'No diseases found!');
      } else {
			$statusCode = 200;


      }
      $requestContentType = $_SERVER['HTTP_ACCEPT'];
  		$this ->setHttpHeaders($requestContentType, $statusCode);

  		if(strpos($requestContentType,'application/json') !== false){
  			$response = $this->encodeJson($stmt);
  			echo $response;
  		} else if(strpos($requestContentType,'text/html') !== false){
        $response = $this->encodeHtml($stmt);
  			echo $response;
  		} else if(strpos($requestContentType,'application/xml') !== false){
      	$response = $this->encodeXml($stmt);
  			echo $response;
  		}
    	}

  	public function encodeHtml($responseData) {

      $htmlResponse = "<table border='1'>";
  		foreach($responseData as $key=>$value) {
			// var_dump($value);
  		    if (!empty($value)) {
      		    $htmlResponse .= "<tr><td>". ($key+1). "</td><td>". ucwords($value["Name"]). "</td></tr>";
          }
  		}
  		$htmlResponse .= "</table>";
  		return "<html>".$htmlResponse."</html>";
  	}

  	public function encodeJson($responseData) {

		// article array
		$diseases_arr=array();
		foreach($responseData as $key=>$value) {
			// var_dump($key, $value["Name"]);
			$jsonResponse = array("Number" => ($key+1), "Disease" => ucwords($value["Name"]));
			array_push($diseases_arr, $jsonResponse);
		  }
		// var_dump( $diseases_arr);
		$jsonResponse = json_encode($diseases_arr, JSON_UNESCAPED_UNICODE);
		// echo $jsonResponse;
  		return $jsonResponse;
  	}

  	public function encodeXml($responseData) {
  		// creating object of SimpleXMLElement
  		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><disease></disease>');
  		foreach($responseData as $key=>$value) {
        $xml1 = $xml->addChild("Nr", ($key+1));
  			$xml1->addChild("Disease",ucwords($value["Name"]));
  		}
  		return $xml->asXML();
  	}
}
?>