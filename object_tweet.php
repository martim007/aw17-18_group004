<?php
class Tweet{

    // database connection and table name
    private $conn;
    private $table_name = "Twitter";

    // object properties
    public $PostDate;
    public $T_ID;
    public $T_URL;
    public $Tweet;
    public $TermCount;


    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

       // read tweets
       function read(){

        // select all query
        $query = "SELECT
                    t.T_ID, t.T_URL
                FROM
                    " . $this->table_name . " t
                    ORDER BY t.T_ID
                    ";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }


    function read_one($T_ID){

        // // query to read single tweet
            $query = "SELECT
                t.T_ID, t.PostDate, t.T_URL, t.Tweet, t.TermCount
                FROM
                " . $this->table_name . " t
                WHERE
                t.T_ID = " . $T_ID . "
                ";

        // // prepare query statement
            $stmt = $this->conn->prepare( $query );

        // // execute query

            $stmt->execute();

            return $stmt;
        }
}
?>