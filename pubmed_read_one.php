<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once 'database.php';
include_once 'object_pubmed.php';
include_once 'SimpleRest.php';

class ReadOne extends SimpleRest {

  public function __construct(){
	}
	function readOne($aid)  {

	// get database connection
	$database = new Database();
	$db = $database->getConnection();
	// prepare articles object
	$pubmed = new Pubmed($db);
	// read the details of articles to be edited
	$stmt = $pubmed->read_one($aid);

  $stmt2 = $pubmed->read_related_dis($aid);

	if (empty($stmt)) {
          $statusCode= 404;
          $stmt = array('error' => 'No id found!');
      } else {
			$statusCode = 200;

      }
      $requestContentType = $_SERVER['HTTP_ACCEPT'];
  		$this ->setHttpHeaders($requestContentType, $statusCode);

  		if(strpos($requestContentType,'application/json') !== false){
  			$response = $this->encodeJson($stmt, $stmt2);
  			echo $response;
  		} else if(strpos($requestContentType,'text/html') !== false){
        $response = $this->encodeHtml($stmt, $stmt2);
  			echo $response;
  		} else if(strpos($requestContentType,'application/xml') !== false){
      	$response = $this->encodeXml($stmt, $stmt2);
  			echo $response;
  		}
    }

  	public function encodeHtml($responseData, $stmt2) {
		if (!empty($responseData)) {
	  $htmlResponse = "<table border='1'>";

	  $htmlResponse .= "<thead><tr><th>Number</th><th>Title</th><th>Authors</th><th>Date</th><th>Abstract</th>";
  		foreach($responseData as $key=>$value) {
        if($value["Date"]=="0001-01-01"){
          $value["Date"]="Unknown";
        }
			// var_dump($value);
  		    if (!empty($value)) {
				  $htmlResponse .= "<tr><td>". ($value["A_ID"]). "</td><td>". ucwords($value["Title"]). "</td><td>".
				  ucwords($value["Authors"]). "</td><td>".($value["Date"]) . "</td><td>".
				  ucwords($value["Abstract"]) ."</td></tr>";
          }
  		}
      $htmlResponse .= "</table>";

      $htmlResponse2 = "<table border='1'><caption>Related Diseases</caption>";
  	  $htmlResponse2 .= "<thead><tr><th>Name</th></tr></thead>";
      foreach($stmt2 as $key=>$value) {
      // var_dump($value);
          if (!empty($value)) {
          $htmlResponse2 .= "<tr><td>". ucwords($value["Name"]). "</td></tr>";
          }
      }
      $htmlResponse2 .= "</table>";
      return "<html>".$htmlResponse."<br>".$htmlResponse2."<br></html>";
  	}
  }

  	public function encodeJson($responseData, $stmt2) {
      $disease_arr = array();
      foreach($responseData as $key=>$value) {
        if($value["Date"]==="0001-01-01"){
          $value["Date"]=="Unknown";
        }
  		    $jsonResponse = array("pmid" => ($value["A_ID"]), "title" => ucwords($value["Title"]), 
                                "authors" => ucwords($value["Authors"]), "date" => $value["Date"], 
                                "abstract" => ucwords($value["Abstract"]));
      }
      array_push($disease_arr, $jsonResponse);
      foreach($stmt2 as $key=>$value) {
  		    $jsonResponse2 = array("did" => ($value[0]), "said" => $value["SA_ID"],
          "diseasename" => $value["Name"]);
      }
      array_push($disease_arr, $jsonResponse2);
      $disease_arr = json_encode($disease_arr, JSON_UNESCAPED_UNICODE);
  		return $disease_arr;
  	}

  	public function encodeXml($responseData, $stmt2) {
  		// creating object of SimpleXMLElement
  		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><article></article>');
	  foreach($responseData as $key => $value) {
        if($value["Date"]=="0001-01-01"){
          $value["Date"]="Unknown";
        }
        $xml->addChild("ID", ($value["A_ID"]));
        $xml->addChild("Title", ucwords($value["Title"]));
        $xml->addChild("Authors", ucwords($value["Authors"]));
        $xml->addChild("Date", ($value["Date"]));
        $xml->addChild("Abstract", ($value["Abstract"]));
  	}
    foreach($stmt2 as $key => $value) {
        $xml->addChild("D_ID", ($value["D_ID"]));
        $xml->addChild("SA_ID", $value["SA_ID"]);
        $xml->addChild("Name", ucwords($value["Name"]));
    }
    return $xml->asXML();
  }
}
?>