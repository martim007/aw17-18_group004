<?php
// required headers
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once 'database.php';
include_once 'object_disease.php';
include_once 'SimpleRest.php';


class SearchOne extends SimpleRest {

	// constructor with $db as database connection
	public function __construct(){
	}

	function searchOne($dname)  {

		// get database connection
		$database = new Database();
		$db = $database->getConnection();

		// prepare disease object
		$disease = new Disease($db);

		// read the details of disease to be edited
		$stmt = $disease->searchdiseases($dname);
		$top_art = $disease->search_top_art($dname);
		$top_pic = $disease->search_top_pic($dname);
		$top_tweet = $disease->search_top_tweet($dname);
		$top_dis = $disease->search_related_dis($dname);
		$wiki = $disease->search_metWiki($dname);
		$field = $disease->search_metField($dname);
		$death = $disease->search_metDeaths($dname);
		$uniprot = $disease->search_metUniprot($dname);
		
		$x = 0;

		foreach($stmt as $key=>$value){
			if ($value["SA_ID"] != Null){
				$x = 1;
			}
		} 
		if ($x != 1) {
	      $statusCode= 404;
			  $stmt = array('error' => 'Disease name not found!');
		} else {
						$statusCode = 200;
	  	}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
	  	$this ->setHttpHeaders($requestContentType, $statusCode, $top_tweet, $top_dis, $wiki, $field, $death, $uniprot);
		
		if ($statusCode == 404){
			echo $statusCode;
			echo " - ";
			echo $stmt["error"];
		} else {
			$stmt = $disease->searchdiseases($dname);
	  		if(strpos($requestContentType,'application/json') !== false){
	  			$response = $this->encodeJson($stmt, $top_art, $top_pic, $top_tweet, $top_dis, $wiki, $field, $death, $uniprot);
	  			echo $response;
	  		} else if(strpos($requestContentType,'text/html') !== false){
	    		$response = $this->encodeHtml($stmt, $top_art, $top_pic, $top_tweet, $top_dis, $wiki, $field, $death, $uniprot);
	  			echo $response;
	  		} else if(strpos($requestContentType,'application/xml') !== false){
	  			$response = $this->encodeXml($stmt, $top_art, $top_pic, $top_tweet, $top_dis, $wiki, $field, $death, $uniprot);
	  			echo $response;
	  		}
		}
	}

	public function encodeHtml($responseData, $top_art,  $top_pic, $top_tweet, $top_dis, $wiki, $field, $death, $uniprot) {

		$htmlResponse = "<table border='1'>";
	  	$htmlResponse .= "<thead><tr><th>Number</th><th>Disease</th></tr></thead>";
		foreach($responseData as $key=>$value) {
	  		$htmlResponse .= "<tr><td>". ($value["SA_ID"]). "</td><td>". ucwords($value["Name"]). "</td></tr>";
	  	}
	  	$htmlResponse .= "</table>";

	  	// tabela top articles
	  	$htmlResponse2 = "<table border='1'><caption>Top Articles</caption>";
	  	$htmlResponse2 .= "<thead><tr><th>PMID</th><th>Title</th></th><th>Abstract</th><th>Date</th></tr></thead>";
		foreach($top_art as $key=>$value) {
			if($value["Date"]=="0001-01-01"){
				$value["Date"]="Unknown";
			}
			$htmlResponse2 .= "<tr><td>". ($value["A_ID"]). "</td><td>". $value["Title"].
												"</td><td>". $value["Abstract"]."</td><td>". $value["Date"]."</td></tr>";
	  	}
		$htmlResponse2 .= "</table>";

		// tabela top pictures
		$htmlResponse3 = "<table border='1'><caption>Top Pictures</caption>";
		$htmlResponse3 .= "<thead><tr><th>URL</th><th>Tags</th></th><th>Date</th></tr></thead>";
	  	foreach($top_pic as $key=>$value) {
			if($value["Date"]=="0001-01-01"){
				$value["Date"]="Unknown";
			}
			$htmlResponse3 .= "<tr><td>". ($value["F_URL"]). "</td><td>". $value["Tags"].
							"</td><td>". $value["Date"]."</td></tr>";
	  	}
	  	$htmlResponse3 .= "</table>";

		// tabela top tweets
		$htmlResponse4 = "<table border='1'><caption>Top Tweets</caption>";
		$htmlResponse4 .= "<thead><tr><th>URL</th><th>Tweet</th></th><th>Date</th></tr></thead>";
		foreach($top_tweet as $key=>$value) {
			if($value["Date"]=="0001-01-01"){
				$value["Date"]="Unknown";
			}
			$htmlResponse4 .= "<tr><td>". ($value["T_URL"]). "</td><td>". $value["Tweet"].
												"</td><td>". $value["PostDate"]."</td></tr>";
			}
		$htmlResponse4 .= "</table>";

	  // tabela top tweets
	  $htmlResponse5 = "<table border='1'><caption>Top Related Diseases</caption>";
	  $htmlResponse5 .= "<thead><tr><th>Disease</th><th>Number of Articles in Common</th></tr></thead>";
	  foreach($top_dis as $key=>$value) {
			$htmlResponse5 .= "<tr><td>". ucwords($value["Name"]). "</td><td>". $value["COUNT(*)"].
	          						"</td></tr>";
	  }
	  $htmlResponse5 .= "</table>";

		// Metadata
	  $htmlResponse6 = "<p>METADATA<br> ";
		$htmlResponse6 .= "Wikipedia page: <br>";
	  foreach($wiki as $key=>$value){
			$htmlResponse6 .= "- ". $value["wiki"]. "<br>";
		}
		$htmlResponse6 .= "Field: <br>";
		foreach($field as $key=>$value){
	  	$htmlResponse6 .= "- ". $value["field"]. "<br>";
		}
		$htmlResponse6 .= "</p>";

		$htmlResponse7 = "<table border='1'><caption>Famous People Killed by this Disease</caption>";
		$htmlResponse7 .= "<thead><tr><th>Name</th><th>Death Date</th></th><th>Death Place</th></tr></thead>";
		foreach($death as $key=>$value){
			if($value["deathdate"]=="0000-00-00"){
				$value["deathdate"]="Unknown";
			}
			$htmlResponse7 .= "<tr><td>". ucwords($value["name"]). "</td><td>". $value["deathdate"]. "</td><td>". $value["deathplace"].
			      						"</td></tr>";
		}
		$htmlResponse7 .= "</table>";

		$htmlResponse8 = "<table border='1'><caption>Genes Related to this Disease</caption>";
		$htmlResponse8 .= "<thead><tr><th>Gene Name</th><th>Gene NCBI ID</th></th><th>Wikidata Link</th></tr></thead>";
		foreach($uniprot as $key=>$value){
			$htmlResponse8 .= "<tr><td>". ($value["GeneLabel"]). "</td><td>". $value["NCBIgeneID"]. "</td><td>". $value["gene"].
												"</td></tr>";
		}
		$htmlResponse8 .= "</table>";

		return "<html>".$htmlResponse."<br>".$htmlResponse2."<br>".$htmlResponse3.
			  		"<br>".$htmlResponse4."<br>".$htmlResponse5."<br>".$htmlResponse6."<br>".$htmlResponse7."<br>".$htmlResponse8."<br></html>";
	}

	public function encodeJson($responseData, $top_art,  $top_pic, $top_tweet, $top_dis, $wiki, $field, $death, $uniprot){
		foreach($responseData as $key=>$value) {
			$jsonArticles = array();
			foreach($top_art as $keyart=>$valueart){
					$jsonTopArt = ($valueart["A_ID"]);
					$jsonTopArticle = array("pmid" => $jsonTopArt);
					array_push($jsonArticles, $jsonTopArticle);
			}
			$jsonPictures = array();
			foreach($top_pic as $keypic=>$valuepic){
					$jsonTopPic = ($valuepic["F_ID"]);
					$jsonTopPicture = array("pic_id" => $jsonTopPic);
					array_push($jsonPictures, $jsonTopPicture);
			}
			$jsonTweets = array();
			foreach($top_tweet as $keytw=>$valuetw){
					$jsonTopTweet = ($valuetw["T_ID"]);
					$jsonTopTweets = array("tweet_id" => $jsonTopTweet);
					array_push($jsonTweets, $jsonTopTweet);
			}
			$jsonRelatedDiseases = array();
			foreach($top_dis as $keydis=>$valuedis){
					$jsonTopDis = ($valuedis["Name"]);
					$jsonTopDiseases = array("rel_name" => $jsonTopDis);
					array_push($jsonRelatedDiseases, $jsonTopDiseases);
			}
			$jsonMetadata = array();
			$jsonWiki = array();
			foreach($wiki as $keyw=>$valuew){
					$jsonW = ($valuew["wiki"]);
					$jsonWikiLinks = array("wikilinks" => $jsonW);
					array_push($jsonWiki, $jsonWikiLinks);
			}
			$jsonFields = array();
			foreach($field as $keyf=>$valuef){
					$jsonF = ($valuef["field"]);
					$jsonField = array("field" => $jsonF);
					array_push($jsonFields, $jsonField);
			}
			$jsonDeaths = array();
			foreach($death as $keyd=>$valued){
					if($valued["deathdate"]=="0000-00-00"){
						$valued["deathdate"]="Unknown";
					}
					$jsonD = array("deaths" => $valued["name"], "deathdate" => $valued["deathdate"], "deathplace" => $valued["deathplace"]);
					$jsonDeath = array($jsonD);
					array_push($jsonDeaths, $jsonDeath);
			}
			$jsonUniProts = array();
			foreach($uniprot as $keyu=>$valueu){
					$jsonUni = array("gene" => $valueu["GeneLabel"], "gene_id" => $valueu["NCBIgeneID"], "wikidata_link" => $valueu["gene"]);
					$jsonUniProt = array($jsonUni);
					array_push($jsonUniProts, $jsonUniProt);
			}
			array_push($jsonMetadata, $jsonWiki);
			array_push($jsonMetadata, $jsonFields);
			array_push($jsonMetadata, $jsonDeaths);
			array_push($jsonMetadata, $jsonUniProts);

			$jsonResponse = array("id" => ($value["SA_ID"]), "disease_name" => ($value["Name"]), "articles" => $jsonArticles,
														"pictures" => $jsonPictures, "tweets" => $jsonTweets, "related_diseases" => $jsonRelatedDiseases,
														"metadata" => $jsonMetadata);
		}
		$jsonResponse = json_encode($jsonResponse, JSON_UNESCAPED_UNICODE);
		return $jsonResponse;
	}

	public function encodeXml($responseData, $top_art,  $top_pic, $top_tweet, $top_dis, $wiki, $field, $death, $uniprot) {
		// creating object of SimpleXMLElement
		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><disease></disease>');
		foreach($responseData as $key=>$value) {
			$disease = $xml->addChild("Disease", $value["Name"]);
		}
		foreach($top_art as $keyart=>$valueart){
			$articles = $disease->addChild("Article", $valueart["A_ID"]);
		}
		foreach($top_pic as $keypic=>$valuepic){
			$pictures = $disease->addChild("Picture", $valuepic["F_ID"]);
		}
		foreach($top_tweet as $keytw=>$valuetw){
			$tweets = $disease->addChild("Tweet", $valuetw["T_ID"]);
		}
		foreach ($top_dis as $keydis => $valuedis) {
			$relatedDis = $disease->addChild("RelatedDisease", $valuedis["D_ID"]);
		}
		$metadata = $disease->addChild("Metadata");
		foreach ($wiki as $keyw => $valuew) {
			$wiki = $metadata->addChild("WikiLink", $valuew["wiki"]);
		}
		foreach ($field as $keyf => $valuef) {
			$field = $metadata->addChild("Field", $valuef["field"]);
		}
		foreach ($death as $keyd => $valued) {
			$death = $metadata->addChild("Death");
			$deathname = $death->addChild("Name", $valued["name"]);
			$deathdate = $death->addChild("Date", $valued["deathdate"]);
			$deathplace = $death->addChild("Place", $valued["deathplace"]);
		}
		foreach ($uniprot as $keyu => $valueu){
			$uniprot = $metadata->addChild("UniProt");
			$gene = $uniprot->addChild("Gene", $valueu["GeneLabel"]);
			$ID = $uniprot->addChild("GeneID", $valueu["NCBIgeneID"]);
			$link = $uniprot->addChild("WikidataLink", $valueu["gene"]);
		}
		return $xml->asXML();
	}
}


?>