<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once 'database.php';
include_once 'object_flickr.php';
include_once 'SimpleRest.php';



class Read extends SimpleRest {

    function readAll() {

      // instantiate database and flickr object
      $database = new Database();
      $db = $database->getConnection();

      // initialize object
      $flickr = new Flickr($db);
      // query flickr
	  $stmt = $flickr->read();


      //$num = $stmt->rowCount();
      if (empty($stmt)) {
          $statusCode= 404;
          $stmt = array('error' => 'No pictures found!');
      } else {
			$statusCode = 200;


      }
      $requestContentType = $_SERVER['HTTP_ACCEPT'];
  		$this ->setHttpHeaders($requestContentType, $statusCode);

  		if(strpos($requestContentType,'application/json') !== false){
  			$response = $this->encodeJson($stmt);
  			echo $response;
  		} else if(strpos($requestContentType,'text/html') !== false){
        $response = $this->encodeHtml($stmt);
  			echo $response;
  		} else if(strpos($requestContentType,'application/xml') !== false){
      	$response = $this->encodeXml($stmt);
  			echo $response;
  		}
    	}

  	public function encodeHtml($responseData) {

      $htmlResponse = "<table border='1'>";
  		foreach($responseData as $key=>$value) {
			// var_dump($value);
  		    if (!empty($value)) {
      		    $htmlResponse .= "<tr><td>". ($value["F_ID"]). "</td><td>". strtolower($value["F_URL"]). "</td></tr>";
          }
  		}
  		$htmlResponse .= "</table>";
  		return "<html>".$htmlResponse."</html>";
  	}

  	public function encodeJson($responseData) {

		// article array
		$flickr_arr=array();
		foreach($responseData as $key=>$value) {
			// var_dump($key, $value["Name"]);
			$jsonResponse = array("ID" => ($value["F_ID"]), "Flickr" => strtolower($value["F_URL"]));
			array_push($flickr_arr, $jsonResponse);
		  }
		// var_dump( $flickr_arr);
		$jsonResponse = json_encode($flickr_arr, JSON_UNESCAPED_UNICODE);
		// echo $jsonResponse;
  		return $jsonResponse;
  	}

  	public function encodeXml($responseData) {
  		// creating object of SimpleXMLElement
  		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><flickr></flickr>');
  		foreach($responseData as $key=>$value) {
  			$xml->addChild(($value["F_ID"]),strtolower($value["F_URL"]));
  		}
  		return $xml->asXML();
  	}
}
?>