<?php
require_once("tweet_read.php");
require_once("tweet_read_one.php");

$view = "";
if(isset($_GET["view"]))
	$view = $_GET["view"];

/*
controls the RESTful services
URL mapping
*/
switch($view){

	case "all":
		// to handle REST Url/tweet/list/

		$read = new Read();
		$read->readAll();
		break;

	case "single":

		// to handle REST Url/tweet/show/<id>/
		$read = new ReadOne();
		$read->readOne($_GET["tid"]);
		
		break;

	case "" :
		//404 - not found;
		break;
}
?>