<?php

// include database and object files
header("Content-Type: application/json; charset=UTF-8");
include_once 'database.php';


global $db;
// get database connection
$database = new Database();
$db = $database->getConnection();

// No javascript: $.post(URL,data,function(data,status,xhr),dataType)
// function likePhotoInServer(id, b) {
//     alert(b); // $.post()
// }



// We need to know if it is implicit or explicit feedback,
// the name of the disease,
// what kind of media (Articles/Pictures/Tweets)!!!! nomes têm de ser estes!!!!,
// the id in the database (cuidado com ids de doenças porque há tabela SAD)
// and b (a boolean) telling us if like(true) or dislike(false) (needs to be true if implicit feedback)

/*
$disease = $_POST["name"];
$feedback = $_POST["feedback"];
$table = $_POST["media"];
$id = $_POST["id"];
$b = $_POST["b"];*/

$json_string = file_get_contents('php://input');
//echo $json_string;
$obj = json_decode($json_string);

$disease = $obj->name;
$feedback = $obj->feedback;
$table = $obj->media;
$id = $obj->id;
$b = $obj->b;


//Find the table D_ID from disease name
function get_D_ID($disease, $db){
    $query = "SELECT d.D_ID
              FROM Diseases d
              WHERE d.Name=\"".$disease."\";";
    $stmt = $db->prepare( $query );
    $stmt->execute();
    foreach($stmt as $key=>$value) {
        $D_ID = $value["D_ID"];
    //    var_dump( $value["D_ID"]);
    }
    return $D_ID;
}

$D_ID = get_D_ID($disease, $db);

//Get table primary key name
if($table=="Articles"){
    $tableID = "A_ID";
} elseif($table=="Pictures"){
    $tableID = "F_ID";
} else{
    // tweet
    $tableID = "T_ID";
}



if($feedback=="implicit"){
    // Update 'Implicit' in corresponding table
    $query = "UPDATE ". $table . 
             " SET Implicit=Implicit+10 	
              WHERE ".$tableID."=".$id." AND D_ID=".$D_ID.";";
    $stmt = $db->prepare( $query );
    $stmt->execute();
    // Update TOTALS
    $query1 = "UPDATE ". $table .
              " SET TOTAL=Explicit*(0.375*TFIDF+0.1*DiShIn+0.5*Implicit+0.025*Date)
               WHERE ".$tableID."=".$id." AND D_ID=".$D_ID.";";
    $stmt1 = $db->prepare( $query1 );
    $stmt1->execute();

} else{
    //explicit
    if($b=="true"){
        // like
        // Update 'Explicit' in corresponding table
        $query = "UPDATE ". $table . 
                 " SET Explicit=POWER(Explicit+1, 4)	
                  WHERE ".$tableID."=".$id." AND D_ID=".$D_ID.";";
        $stmt = $db->prepare( $query );
        $stmt->execute();
        // Update TOTALS
        $query1 = "UPDATE ". $table .
                  " SET TOTAL=Explicit*(0.375*TFIDF+0.1*DiShIn+0.5*Implicit+0.025*Date)
                   WHERE ".$tableID."=".$id." AND D_ID=".$D_ID.";";
        $stmt1 = $db->prepare( $query1 );
        $stmt1->execute();
    } else{
        // dislike
        // Update 'Explicit' in corresponding table
        $query = "UPDATE ". $table . 
                 " SET Explicit=0 	
                  WHERE ".$tableID."=".$id." AND D_ID=".$D_ID.";";
        $stmt = $db->prepare( $query );
        $stmt->execute();
        // Update TOTALS
        $query1 = "UPDATE ". $table .
                  " SET TOTAL=Explicit*(0.375*TFIDF+0.1*DiShIn+0.5*Implicit+0.025*Date)
                   WHERE ".$tableID."=".$id." AND D_ID=".$D_ID.";";
        $stmt1 = $db->prepare( $query1 );
        $stmt1->execute();
    }

}


?>