<?php
class Statistics{

    // database connection and table name
    private $conn;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read products
    function read_dis(){

    // select all query
    $query = "SELECT
    (SELECT COUNT(L_ID) FROM DiseaseList) as count_1,
    (SELECT COUNT(D_ID) FROM Diseases) as count_2,
    (SELECT COUNT(A_ID) FROM PubMed) as count_3,
    (SELECT COUNT(T_ID) FROM Twitter) as count_4,
    (SELECT COUNT(F_ID) FROM Flickr) as count_5,
    (SELECT COUNT(M_ID) FROM Metadata) as count_6,
    (SELECT COUNT(G_ID) FROM UniProt) as count_7;";
    // prepare query statement
    $stmt = $this->conn->prepare($query);

    // execute query
    $stmt->execute();
    return $stmt;
    }

    function best_art_score(){
    $query = "SELECT d.Name, a.TOTAL FROM Diseases d, Articles a WHERE (d.D_ID,a.TOTAL) =
    (SELECT D_ID, TOTAL FROM Articles WHERE TOTAL = (SELECT MAX(TOTAL) FROM Articles LIMIT 1))";
    // prepare query statement
    $stmt= $this->conn->prepare($query);
    // execute query
    $stmt->execute();
    return $stmt;
    }

    function best_tweet_score(){
    $query = "SELECT d.Name, t.TOTAL FROM Diseases d, Tweets t WHERE (d.D_ID,t.TOTAL) =
    (SELECT D_ID, TOTAL FROM Tweets WHERE TOTAL = (SELECT MAX(TOTAL) FROM Tweets LIMIT 1))";
    // prepare query statement
    $stmt= $this->conn->prepare($query);
    // execute query
    $stmt->execute();
    return $stmt;
    }

    function best_pic_score(){
    $query = "SELECT d.Name, p.TOTAL FROM Diseases d, Pictures p WHERE (d.D_ID,p.TOTAL) =
    (SELECT D_ID, TOTAL FROM Pictures WHERE TOTAL = (SELECT MAX(TOTAL) FROM Pictures LIMIT 1))";
    // prepare query statement
    $stmt= $this->conn->prepare($query);
    // execute query
    $stmt->execute();
    return $stmt;
    }

    ######EXAMPLE: LUNG CANCER
    function top_5_articles(){
    $query = "SELECT p.Title, a.TOTAL FROM Diseases d, Articles a, PubMed p WHERE d.D_ID = a.D_ID AND a.A_ID = p.A_ID AND
    d.Name = 'lung cancer' ORDER BY a.TOTAL DESC LIMIT 5";
    // prepare query statement
    $stmt= $this->conn->prepare($query);
    // execute query
    $stmt->execute();
    return $stmt;
    }
}
?>