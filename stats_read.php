<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once 'database.php';
include_once 'stats2.php';
include_once 'SimpleRest.php';

class Stats extends SimpleRest {

    function readAll() {

      // instantiate database and disease object
      $database = new Database();
      $db = $database->getConnection();

      // initialize object
      $data = new Statistics($db);

      // query disease
	    $result = $data->read_dis();
      $result1 = $data->best_art_score();
      $result2 = $data->best_pic_score();
      $result3 = $data->best_tweet_score();
      $result4 =$data->top_5_articles();

      //$num = $stmt->rowCount();
      if (empty($result)) {
          $statusCode= 404;
          $result = array('error' => 'No diseases found!');
      } else {
			$statusCode = 200;
    }
    $requestContentType = $_SERVER['HTTP_ACCEPT'];
  	$this ->setHttpHeaders($requestContentType, $statusCode);

  	if(strpos($requestContentType,'application/json') !== false){
  			$response = $this->encodeJson($stmt);
  			echo $response;
  	}

		// TOTAL DATA array
		$result_arr = array();
    foreach ($result as $key=>$value) {
      $jsonResponse = array("disease_nr" => $value[0], "available_dis" => $value[1],
     "articles_nr" => $value[2], "tweets_nr" => $value[3], "pics_nr" => $value[4],
     "metadata_nr" => $value[5], "genes_nr" => $value[6]);
			array_push($result_arr, $jsonResponse);
	  }

    // BEST DATA array
    foreach ($result1 as $key=>$value) {
      $jsonResponse = array("best_article_name" => $value[0], "score" => $value[1]);
      array_push($result_arr, $jsonResponse);
    }
    foreach ($result2 as $key=>$value) {
      $jsonResponse = array("best_pic_name" => $value[0], "score" => $value[1]);
      array_push($result_arr, $jsonResponse);
    }
    foreach ($result3 as $key=>$value) {
      $jsonResponse = array("best_tweet_name" => $value[0], "score" => $value[1]);
      array_push($result_arr, $jsonResponse);
    }
    $article = array();
    foreach ($result4 as $key=>$value) {
      $jsonResponse = array("article_title" => $value[0], "score" => $value[1]);
      array_push($article, $jsonResponse);
    }
    array_push($result_arr, $article);
    $result_arr = json_encode($result_arr, JSON_UNESCAPED_UNICODE);
    print $result_arr;
  }
}
?>