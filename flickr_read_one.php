<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once 'database.php';
include_once 'object_flickr.php';
include_once 'SimpleRest.php';


class ReadOne extends SimpleRest {

  public function __construct(){
  }

	function readOne($fid)  {
	// get database connection
	$database = new Database();
	$db = $database->getConnection();


	// prepare pictures object
	$flickr = new Flickr($db);


	// read the details of pictures to be edited
	$stmt = $flickr->read_one($fid);
	// echo($id);
	// print_r($stmt);
	$x = 0;


    foreach($stmt as $key=>$value){
      if ($value["F_ID"] != Null){
        $x = 1;
      }
    } 
    if ($x != 1) {
        $statusCode= 404;
        $stmt = array('error' => 'Disease name not found!');
    } else {
            $statusCode = 200;
      }

    $requestContentType = $_SERVER['HTTP_ACCEPT'];
      $this ->setHttpHeaders($requestContentType, $statusCode, $top_tweet, $top_dis, $wiki, $field, $death, $uniprot);
    
    if ($statusCode == 404){
      echo $statusCode;
      echo " - ";
      echo $stmt["error"];
    } else {
        $stmt = $flickr->read_one($fid);
        if(strpos($requestContentType,'application/json') !== false){
          $response = $this->encodeJson($stmt, $top_art, $top_pic, $top_tweet, $top_dis, $wiki, $field, $death, $uniprot);
          echo $response;
        } else if(strpos($requestContentType,'text/html') !== false){
          $response = $this->encodeHtml($stmt, $top_art, $top_pic, $top_tweet, $top_dis, $wiki, $field, $death, $uniprot);
          echo $response;
        } else if(strpos($requestContentType,'application/xml') !== false){
          $response = $this->encodeXml($stmt, $top_art, $top_pic, $top_tweet, $top_dis, $wiki, $field, $death, $uniprot);
          echo $response;
        }
    }
  }

  	public function encodeHtml($responseData) {
		if (!empty($responseData)) {
	  $htmlResponse = "<table border='1'>";

	  $htmlResponse .= "<thead><tr><th>ID</th><th>Date</th><th>URL</th><th>Tags</th><th>Term Count</th></tr></thead>";
  		foreach($responseData as $key=>$value) {
			// var_dump($value);
  		    if (!empty($value)) {
				  $htmlResponse .= "<tr><td>". ($value["F_ID"]). "</td><td>". ($value["Date"]). "</td><td>".
				  ($value["F_URL"]). "</td><td>".($value["Tags"]) . "</td><td>".
				  ucwords($value["TermCount"]) ."</td></tr>";
          }
  		}
  		$htmlResponse .= "</table>";
		  return "<html>".$htmlResponse."</html>";
	}
  	}

  	public function encodeJson($responseData) {

		// article array
		foreach($responseData as $key=>$value) {
      if($value["Date"]=="0001-01-01"){
        $value["Date"]="Unknown";
      }
      $jsonResponse = array("pic_id" => ($value["F_ID"]), "date" => ($value["Date"]), "src" => ($value["F_URL"]),
                            "tags" => ($value["Tags"]), "term_count" => ($value["TermCount"]));
      $jsonResponse = json_encode($jsonResponse, JSON_UNESCAPED_UNICODE);
		}
		// echo $jsonResponse;
  		return $jsonResponse;
  	}

  	public function encodeXml($responseData) {
  		// creating object of SimpleXMLElement
  		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><flickr></flickr>');
  		foreach($responseData as $key=>$value) {
        if($value["Date"]=="0001-01-01"){
          $value["Date"]="Unknown";
        }
		$xml->addChild("ID", ($value["F_ID"]));
        $xml->addChild("URL", strtolower($value["F_URL"]));
        $xml->addChild("Tags", ($value["Tags"]));
        $xml->addChild("Term Count", ($value["TermCount"]));
		$xml->addChild("Date", ($value["Date"]));
  		}
  		return $xml->asXML();
 	}
}
?>