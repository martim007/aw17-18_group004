<?php
require_once("pubmed_read.php");
require_once("pubmed_read_one.php");

$view = "";
if(isset($_GET["view"]))
	$view = $_GET["view"];

/*
controls the RESTful services
URL mapping
*/
switch($view){

	case "all":
		// to handle REST Url /article/list/

		$read = new Read();
		$read->readAll();
		break;
	
	case "single":
	
		// to handle REST Url /article/show/<id>/
		$read = new ReadOne();
		$read->readOne($_GET["aid"]);
		
		break;

	case "" :
		//404 - not found;
		break;
}
?>