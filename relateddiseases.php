<?php

// include database and object files
header("Content-Type: application/json; charset=UTF-8");
include_once 'database.php';


global $db;
// get database connection
$database = new Database();
$db = $database->getConnection();

$json_string = file_get_contents('php://input');
//echo $json_string;
$obj = json_decode($json_string);

$D_ID = $obj->d_id;
$id = $obj->pmid;
$table="Articles";
$tableID = "A_ID";

// Update 'Explicit' in corresponding table
        $query = "UPDATE ". $table . 
                 " SET Explicit=0 	
                  WHERE ".$tableID."=".$id." AND D_ID=".$D_ID.";";
        $stmt = $db->prepare( $query );
        $stmt->execute();
        // Update TOTALS
        $query1 = "UPDATE ". $table .
                  " SET TOTAL=Explicit*(0.375*TFIDF+0.1*DiShIn+0.5*Implicit+0.025*Date)
                   WHERE ".$tableID."=".$id." AND D_ID=".$D_ID.";";
        $stmt1 = $db->prepare( $query1 );
        $stmt1->execute();

?>