<?php
class Disease{

    // database connection and table name
    private $conn;
    private $table_name = "Diseases";


    // object properties
    public $D_ID;
    public $Name;



    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read products
    function read(){

       // select all query
        $query = "SELECT
                    d.Name
                FROM
                    " . $this->table_name . " d ORDER BY d.Name ASC
                    ";
//d.ArticlesNumber, d.D_ID, d.TweetsNumber

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function read_one($D_ID){

// // query to read single disease
            $query = "SELECT
             d.Name, d.D_ID, sad.SA_ID
               FROM
             " . $this->table_name . " d, SortAlphabeticalDis sad
              WHERE
             sad.SA_ID = " . $D_ID . " AND sad.D_ID=d.D_ID
             ";

// // prepare query statement
            $stmt = $this->conn->prepare( $query );

// // bind id of product to be updated
        // $stmt->bindParam(1, $this->D_ID);

// // execute query

            $stmt->execute();

            return $stmt;

    }

    function read_top_art($D_ID){
        $query = "SELECT p.Title, p.Abstract, p.Authors, p.Date, p.A_ID
    	FROM Diseases d, Articles a, PubMed p, SortAlphabeticalDis sad
    	WHERE sad.SA_ID = " . $D_ID . " AND sad.D_ID=d.D_ID AND d.D_ID=a.D_ID AND a.A_ID = p.A_ID AND a.Explicit>0
	    ORDER BY a.TOTAL DESC
    	LIMIT 5;";
        // // prepare query statement
        $stmt = $this->conn->prepare( $query );
        // // execute query

        $stmt->execute();
        return $stmt;

    }

    function read_top_pic($D_ID){
        $query="SELECT f.F_URL, f.Tags , f.Date
        FROM Diseases d, Pictures p, Flickr f, SortAlphabeticalDis sad
        WHERE sad.SA_ID = " . $D_ID . " AND sad.D_ID=d.D_ID AND d.D_ID=p.D_ID AND p.F_ID=f.F_ID AND p.Explicit>0
        ORDER BY p.TOTAL DESC
        LIMIT 5;";
        // // prepare query statement
        $stmt = $this->conn->prepare( $query );
        // // execute query

        $stmt->execute();
        return $stmt;
    }

    function read_top_tweet($D_ID){
        $query="SELECT tt.T_URL, tt.Tweet, tt.PostDate
    	FROM Diseases d, Twitter tt, Tweets ee, SortAlphabeticalDis sad
    	WHERE sad.SA_ID = " . $D_ID . " AND sad.D_ID=d.D_ID AND d.D_ID=ee.D_ID AND ee.T_ID=tt.T_ID AND ee.Explicit>0
	    ORDER BY ee.TOTAL DESC
    	LIMIT 5;";
        // // prepare query statement
        $stmt = $this->conn->prepare( $query );
        // // execute query

        $stmt->execute();
        return $stmt;
    }

    function read_related_dis($D_ID){
        $query= "SELECT COUNT(*), d2.Name
        FROM Diseases d2, Articles a, SortAlphabeticalDis sad
        WHERE sad.SA_ID!=" . $D_ID . " AND sad.D_ID=d2.D_ID AND d2.D_ID=a.D_ID AND a.A_ID IN (
        -- Selecionar PMIDs para doença pesquisada
        SELECT a.A_ID
        FROM Diseases d1, Articles a, SortAlphabeticalDis sad
        WHERE sad.SA_ID = " . $D_ID . " AND sad.D_ID=d1.D_ID and d1.D_ID=a.D_ID)

        GROUP BY d2.Name
        ORDER BY COUNT(*) DESC
        LIMIT 10;";
        // // prepare query statement
        $stmt = $this->conn->prepare( $query );
        // // execute query

        $stmt->execute();
        return $stmt;

    }

    function read_metWiki($D_ID){

        $query= "SELECT DISTINCT m.wiki
        FROM Metadata m, DBPedia dbp, Diseases d, SortAlphabeticalDis sad
        WHERE sad.SA_ID = " . $D_ID . " AND sad.D_ID=d.D_ID AND d.D_ID=dbp.D_ID AND dbp.M_ID=m.M_ID;";
        // // prepare query statement
        $stmt1 = $this->conn->prepare( $query );
        // // execute query

        $stmt1->execute();

		    return $stmt1;
    }

    function read_metField($D_ID){
          $query= "SELECT DISTINCT m.field
          FROM Metadata m, DBPedia dbp, Diseases d, SortAlphabeticalDis sad
          WHERE sad.SA_ID = " . $D_ID . " AND sad.D_ID=d.D_ID AND d.D_ID=dbp.D_ID AND dbp.M_ID=m.M_ID;";
          // // prepare query statement
          $stmt2 = $this->conn->prepare( $query );
          // // execute query

          $stmt2->execute();

      return $stmt2;
    }

    function read_metDeaths($D_ID){
          $query= "SELECT DISTINCT m.name, m.deathdate, m.deathplace
          FROM Metadata m, DBPedia dbp, Diseases d, SortAlphabeticalDis sad
          WHERE sad.SA_ID = " . $D_ID . " AND sad.D_ID=d.D_ID AND d.D_ID=dbp.D_ID AND dbp.M_ID=m.M_ID
          ORDER BY m.deathdate DESC
          Limit 10;";
          // // prepare query statement
          $stmt3 = $this->conn->prepare( $query );
          // // execute query

          $stmt3->execute();

      return $stmt3;
    }

    function read_metUniprot($D_ID){
          $query= "SELECT DISTINCT u.GeneLabel, u.gene, u.NCBIgeneID
          FROM UniProt u, GeneInfo g, Diseases d, SortAlphabeticalDis sad
          WHERE sad.SA_ID = " . $D_ID . " AND sad.D_ID=d.D_ID AND d.D_ID=g.D_ID AND g.G_ID = u.G_ID
          Limit 10;";
          // // prepare query statement
          $stmt4 = $this->conn->prepare( $query );
          // // execute query

          $stmt4->execute();

      return $stmt4;
    }

    // search diseases
    function search($keywords){

        // select all query
        // $query = "SELECT
        //             d.Name, d.ArticlesNumber, d.D_ID, d.TweetsNumber
        //         FROM
        //             " . $this->table_name . " d
        //         WHERE
        //             d.Name LIKE ? ";
        //
        // // prepare query statement
        // $stmt = $this->conn->prepare($query);
        //
        // // sanitize
        // $keywords=htmlspecialchars(strip_tags($keywords));
        // $keywords = "%{$keywords}%";
        //
        // // bind
        // $stmt->bindParam(1, $keywords);
        // // $stmt->bindParam(2, $keywords);
        // // $stmt->bindParam(3, $keywords);
        //
        // // execute query
        // $stmt->execute();
        //
        // return $stmt;
    }

}

?>