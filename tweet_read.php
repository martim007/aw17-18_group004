<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once 'database.php';
include_once 'object_tweet.php';
include_once 'SimpleRest.php';



class Read extends SimpleRest {

    function readAll() {

      // instantiate database and tweet object
      $database = new Database();
      $db = $database->getConnection();

      // initialize object
      $tweet = new Tweet($db);
      // query tweet
	  $stmt = $tweet->read();


      //$num = $stmt->rowCount();
      if (empty($stmt)) {
          $statusCode= 404;
          $stmt = array('error' => 'No tweets found!');
      } else {
			$statusCode = 200;


      }
      $requestContentType = $_SERVER['HTTP_ACCEPT'];
  		$this ->setHttpHeaders($requestContentType, $statusCode);

  		if(strpos($requestContentType,'application/json') !== false){
  			$response = $this->encodeJson($stmt);
  			echo $response;
  		} else if(strpos($requestContentType,'text/html') !== false){
        $response = $this->encodeHtml($stmt);
  			echo $response;
  		} else if(strpos($requestContentType,'application/xml') !== false){
      	$response = $this->encodeXml($stmt);
  			echo $response;
  		}
    	}

  	public function encodeHtml($responseData) {

      $htmlResponse = "<table border='1'>";
  		foreach($responseData as $key=>$value) {
			// var_dump($value);
  		    if (!empty($value)) {
      		    $htmlResponse .= "<tr><td>". ($value["T_ID"]). "</td><td>". strtolower($value["T_URL"]). "</td></tr>";
          }
  		}
  		$htmlResponse .= "</table>";
  		return "<html>".$htmlResponse."</html>";
  	}

  	public function encodeJson($responseData) {

		// article array
		$tweet_arr=array();
		foreach($responseData as $key=>$value) {
			// var_dump($key, $value["Name"]);
			$jsonResponse = array("ID" => ($value["T_ID"]), "URL" => strtolower($value["T_URL"]));
			array_push($tweet_arr, $jsonResponse);
		  }
		// var_dump( $tweet_arr);
		$jsonResponse = json_encode($tweet_arr, JSON_UNESCAPED_UNICODE);
		// echo $jsonResponse;
  		return $jsonResponse;
  	}

  	public function encodeXml($responseData) {
  		// creating object of SimpleXMLElement
  		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><tweet></tweet>');
  		foreach($responseData as $key=>$value) {
        $xml1 = $xml->addChild("Nr", ($key+1));
  			$xml1->addChild("ID",($value["T_ID"]));
        $xml1->addChild("URL",strtolower($value["T_URL"]));
  		}
  		return $xml->asXML();
  	}
}
?>