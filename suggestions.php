<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once 'database.php';
include_once 'object_disease.php';

// get the query string "q" parameter from URL
$q = $_REQUEST["q"];
$t = $_REQUEST["t"];
if ($t == ""){
	$t = "5";
}

if ($q == ""){
	echo "[]";
} else {

	// instantiate database and disease object
	$database = new Database();
	$db = $database->getConnection();

	// get the names of all diseases
	$query = "SELECT d.Name FROM Diseases d WHERE d.Name LIKE '" . $q . "%' ORDER BY d.Name ASC LIMIT " . $t . ";";

	// prepare query statement
	$stmt = $db->prepare($query);
	// execute query
	$stmt->execute();

	// read query results
	$num = $stmt->rowCount();

	$a = array();
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	    $a[] = htmlspecialchars( $row['Name'], ENT_NOQUOTES, 'UTF-8' );
	}

	echo json_encode($a);

}
?>