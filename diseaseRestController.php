<?php
require_once("disease_read.php");
require_once("disease_read_one.php");
require_once("disease_search.php");

$view = "";
if(isset($_GET["view"]))
	$view = $_GET["view"];

/*
controls the RESTful services
URL mapping
*/
switch($view){

	case "all":
		// to handle REST Url /disease
		$read = new Read();
		$read->readAll();
		break;

	case "single":
		// to handle REST Url /disease/<id>
		$read = new ReadOne();
		$read->readOne($_GET["id"]);
		break;

  	case "byname":
		// to handle REST Url /disease/<dname>
    	$read = new SearchOne();
  		$read->searchOne(htmlspecialchars($_GET["dname"]));
    	break;

	case "" :
		//404 - not found;
		break;
}
?>