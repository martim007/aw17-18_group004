var suggestions = [];
var dname = [];
var media = [];

function createMetadata(data) {
    var relateddis = data.related_diseases;
    var metadata = data.metadata;
    var metaWiki = metadata[0];
    var tamanhoWiki = metaWiki.length;
    if (tamanhoWiki >= 1){
        var i;
        var wikilink = [];
        for (i=0; i<tamanhoWiki; i++){
            wikilink.push(metaWiki[i].wikilinks);
        }
    }
    var metafield = metadata[1];
    var tamanhoField = metafield.length;
    console.log(metafield);
    if (tamanhoField >= 1){
        var i;
        var fields = [];
        for (i=0; i<tamanhoField; i++){
            fields.push(metafield[i].field)
        }
    } else{
        var fields = "No fields registered for this disease.";
    }
    var tamanhoRel = relateddis.length;
    if (tamanhoRel >= 1){
        var i;
        var rel = [];
        for (i=0; i<tamanhoRel; i++){
            rel.push(relateddis[i].rel_name);
        }
    }else{
        var rel = "No related diseases registered for this disease.";
    }
    var metaDeath = metadata[2];
    var tamanhoDeath = metaDeath.length;
    if (tamanhoDeath >= 1){
        var i;
        var deathnames = [];
        var deathdates = [];
        var deathplaces = [];
        for (i=0; i<tamanhoDeath; i++){
            deathnames.push(metaDeath[i][0].deaths);
            deathplaces.push(metaDeath[i][0].deathplace);
        }
    }else{
        var deathnames = "No famous people deaths registered for this disease.";
        var deathplaces = "No places registered for this disease.";
    }
    var metaGenes = metadata[3];
    var tamanhoGenes = metaGenes.length;
    if (tamanhoGenes >= 1){
        var i;
        var genename = [];
        var geneid = [];
        var genewikidata = [];
        for (i=0; i<tamanhoGenes; i++){
            genename.push(metaGenes[i][0].gene);
        }
    }else{
        var genename = "No genes registered for this disease.";
    }
    return $("<div>")
        .attr("id","resultmetadata")
        .append(
            $("<div>")
                .attr("class","searchname")
                .text(dname),
            $("<div>")
                .attr("class","metadatawiki")
                .append(
                    $("<a>").attr("href",wikilink).text("wikipedia link"),
                    $("<p>")
                ),
            $("<div>")
                .attr("class","metadatacampo")
                .append(
                    $("<strong>").text("Field"),
                    $("<p>").text(fields),
                    $("<p>")
                ),
            $("<div>")
                .attr("class","metadatapessoa")
                .append(
                    $("<strong>").text("Deaths"),
                    $("<p>").text(deathnames),
                    $("<p>")
                ),
            $("<div>")
                .attr("class","metadatalocal")
                .append(
                    $("<strong>").text("Places"),
                    $("<p>").text(deathplaces),
                    $("<p>")
                ),
            $("<div>")
                .attr("class", "metadatagene")
                .append(
                    $("<strong>").text("Genes"),
                    $("<p>").text(genename),
                    $("<p>")
                ),
            $("<div>")
                .attr("class", "relateddiseasestitle")
                .append(
                    $("<strong>").text("Related Diseases")
                ),
            $("<div>")
                .attr("class", "relateddiseases")
                .append(
                    $("<p>").text(rel),
                    $("<p>")
                ),
            );
}

function clearData() {
    $("#resultphotos").html("");
    $("#resultabstracts").html("");
    $("#resulttweets").html("");
    AllAbstracts = [];
    AllTweets = [];
}


var authors = [];
var AllAbstracts = [];
var AllTweets = [];


function createAllAbstracts(articles) {
    getArticleText(articles);
    return $('<ul>').append($.map(AllAbstracts, createAbstract));
}

function getArticleText(articles){
    var i;
    for (i=0; i < articles.length; i++){
        getArticle(articles[i].pmid);
    }
}

function getArticle(pmid){
    $.ajax({
        url:  "http://appserver.alunos.di.fc.ul.pt/~aw004/rest/article/"+pmid,
        type: "GET",
        dataType: "json",
        async: false,
        success: function(article) {
                    var artigo = {};
                    artigo["id"] = article[0].pmid;
                    artigo["title"] = article[0].title;
                    var abs = article[0].abstract;
                    var litAbstract = abs.substring(0,51);
                    var summary = litAbstract.concat("...");
                    artigo["summary"] = summary;
                    artigo["text"] = abs;
                    artigo["authors"] = article[0].authors;
                    artigo["relateddis"] = article[1].diseasename;
                    artigo["relateddisID"] = article[1].did;
                    AllAbstracts.push(artigo);

                  },
        error: function() { alert("Error on obtaining the article");
        }
    });
}

function createAbstract(a) {
    var media = "Articles";
    return $("<li>")
    .attr("class","resultitem")
    .append(
        $("<div>")
            .attr("class","articletitle")
            .append(
                $("<br>"),
                $("<em>").text(a.title)
            ),
        $("<div>")
            .attr("class","articleauthors")
            .append(
                $("<details>").text(a.authors)
            ),
        $("<div>")
            .attr("class","articlesummary")
            .text(a.summary)
            .click(function() {
                $(this).toggle(); 
                $(this).next().toggle(); 
                feedbackImplicit(a.id, media);
                HyperAbstract();
            }),
        $("<div>")
            .attr("class","articleabstract")
            .css("display","none")
            .text(a.text)
            .click(function() { 
                $(this).toggle(); 
                $(this).prev().toggle();
            }),
        createLikeButtons(a.id, media),
        $("<div>")
            .attr("class","articlerelateddis")
            .append(
                $("<a>").text(a.relateddis),
                $("<strong>").text("\t"+"X")
                .click(function() {
                    alert("Thank you for your feedback");
                    excludeRelatedDisease(a.id, a.relateddisID);
                   
                })
            )
        );
}

function excludeRelatedDisease(pmid, id_dis){
    $.ajax({
        type: "POST",
        url: "http://appserver.alunos.di.fc.ul.pt/~aw004/rest/relateddiseases.php",
        // The key needs to match your method's input parameter (case-sensitive).
        data: JSON.stringify({pmid: pmid, d_id: id_dis}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(){
                console.log("success");
            },
        failure: function(errMsg) {
                alert(errMsg);
            }
    });
}

// vamos tratar de tudo de fotos aqui
// vamos criar lis para cada id de cada foto e depois a medida que vão ser carregados vão sendo direcionados para o seu id
// vai juntar todas as fotos na mesma função para depois fazer o display
function createAllPhotos(pictures) {
    getPictureUrl(pictures);
    return $('<ul>').append($.map(pictures, createEmptyPhoto));
}

// vai buscar o url de cada id
function getPictureUrl(pictures){
    for (var i=0; i < pictures.length; i++){
        getPicture(pictures[i].pic_id);
    }
}

// vai buscar os id de cada fotografia
function getPicture(id){
    $.ajax({
        url:  "http://appserver.alunos.di.fc.ul.pt/~aw004/rest/picture/"+id,
        type: "GET",
        dataType: "json",
        success: function(data) {
                    putImgPhoto(data);
                 },
        error: function() { 
            alert("Error on obtaining the picture");
        }
    });
}


//vai colocar o url da foto no seu id
function putImgPhoto(pho) {
    $('#photo'+pho.pic_id).prepend(
        $("<img>")
        .attr("src", pho.src)
    )
}

// vai criar um div para cada id
function createEmptyPhoto(pho){
    var media = "Pictures";
    window.a = pho;
    return $("<li>")
        .attr("id","photo"+pho.pic_id)
        .attr("class","photo")
        .append(
            $("<div>")
                .attr("class","photobuttons")
                .append(createLikeButtons(pho.pic_id, media))
        );
}

//vamos tratar dos tweets aqui, são apenas apresentados quando estão todos disponiveis asincrono
//vai juntar todos os tweets na mesma função para depois fazer o display
function createAllTweets(tweets) {
    getTweets(tweets);
    return $('<ul>').append($.map(AllTweets, createTweet));
}

function getTweets(tweets){
    for (var i=0; i < tweets.length; i++){
        getTweet(tweets[i]);
    }
}

// vai buscar os ids dos tweets ( caso tenha sucesso vai apresentar os tweets embebidos
function getTweet(id){
    $.ajax({
        url:  "http://appserver.alunos.di.fc.ul.pt/~aw004/rest/twitter/"+id,
        type: "GET",
        dataType: "json",
        async: false,
        success: function(tweet) {
                    AllTweets.push({src: tweet.src, id: tweet.tweet_id});
                  },
        error: function() { 
            alert("Error on obtaining the tweet");
        }
    });
}

//vai criar o tweet embebido através dos urls que estamos a passar
function createTweet(t){
    var media = "Tweets";
    return $("<div>").append(
                $("<p>"),
                $("<blockquote>")
                .attr("class", "twitter-tweet")
                .append(
                    $("<a>")
                        .attr("href", t.src),
                    $("<a>")
                        .attr("href", t.src)
                ),
                $("<div>")
                .attr("class","photobuttons")
                .append(createLikeButtons(t.id, media))
            );
}

function feedbackExplicit(id, b, media) {
    var feedback = "explicit";
    $.ajax({
        type: "POST",
        url: "http://appserver.alunos.di.fc.ul.pt/~aw004/rest/feedback.php",
        // The key needs to match your method's input parameter (case-sensitive).
        data: JSON.stringify({ name: dname, feedback: feedback, media: media, id: id, b: b }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(){
            console.log("success");
        },
        failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function feedbackImplicit(id, media) {
    var b = 'true';
    var feedback = "implicit";
    $.ajax({
        type: "POST",
        url: "http://appserver.alunos.di.fc.ul.pt/~aw004/rest/feedback.php",
        // The key needs to match your method's input parameter (case-sensitive).
        data: JSON.stringify({ name: dname, feedback: feedback, media: media, id: id, b: b }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(){
            console.log("success");
        },
        failure: function(errMsg) {
            alert(errMsg);
        }
    });
}

function createLikeButtons (id, media) {
    return $("<div>")
        .attr("class", "likebuttons")
        .append(
            $("<div>")
                .attr("class","likephoto")
                .click(function () {
                    feedbackExplicit(id,true,media);
                    alert("Thank you for your feedback");
                }),
            $("<div>")
                .attr("class","dislikephoto")
                .click(function () {
                    feedbackExplicit(id,false,media);
                    alert("Thank you for your feedback");
                })
        );
}

function getData(text){
    $.ajax({
        url:  "http://appserver.alunos.di.fc.ul.pt/~aw004/rest/search/"+text,
        type: "GET",
        dataType: "json",
        success: function(data) {
                    console.log(data);
                    displayData(data);
                  },
        error: function() { 
                    alert("Error on obtaining disease");
                }
    });
}

// vai apresentar todos os campos calculados anteriormente
function displayData(data) {
    clearData(); // apagar tudo de dentro das caixas.

    // Para que não dê erro se não houver respostas
    if( ! data ) {
        $("#messagebox").prepend("Disease not found, try again.");
    }
    
    dname = (data.disease_name);

    var metadata = data.metadata;
    var articles = data.articles;
    var pictures = data.pictures;
    var tweets = data.tweets;

    if (metadata && metadata.length > 0) {
        console.log("Loading metadata");
        $("#resultphotos").prepend(createMetadata(data));
    } 
    else {
        $("#resultphotos").prepend("No metadata available at this moment.");
    }
    if (articles && articles.length > 0){
        console.log("Loading articles");
        $("#resultabstracts").append(createAllAbstracts(data.articles));
    }
    else{
        $("#resultabstracts").append("No articles available at this moment.");
    }
    if (pictures && pictures.length > 0){
        console.log("Loading pictures");
        $("#resultphotos").append(createAllPhotos(data.pictures));
    }
    else{
        $("#resultphotos").append("No pictures available at this moment.");
    }
    if (tweets && tweets.length > 0){
        console.log("Loading tweets");
        $("#resulttweets").append(createAllTweets(data.tweets));
        twttr.widgets.load();
    }
    else{
        $("#resulttweets").append("No tweets available at this moment.");
    }
    HyperRelatedDis();
    HyperMetaDis();
}

//vai apresentar as sugestões
function displaySuggestions() {
    var box = $('.suggestiontext');

    if( suggestions.length == 0 ) {
        box.html("");
        box.hide();
    }
    else {
        box.html(
            $.map(suggestions, function(s) { 
                return $('<option>').val(s); 
            })
        );
        box.show();
    }
}

//vai calcular as sugestões para cada letra
function getSuggestions(text) {
    $.ajax({
        url: "http://appserver.alunos.di.fc.ul.pt/~aw004/rest/suggestions.php?q="+ text,
        type: "GET",
        dataType: "json",
        success: function(data) {
            suggestions = data;
            displaySuggestions();
        },
        error: function() { 
                    alert("Error on suggestions");
                }
    });
}

// vai fazer que o search inicial
function refreshBig(e) {
    var text = $('#searchbigtext').val();
    refreshText(text);
}

var timer;
function refreshText(text) {
    clearTimeout(timer);
    getData(text);// get the data from server
    $('#searchtext').val(text);
    $('#searchwithresults').show();
    $('#searchsuggestions').hide();
    timer = setTimeout(function(){
        refreshText(text);}
        ,30000); //faz um update de 30 em 30 segundos
}

// vai apresentar as sugesões a não ser que se carregue no enter
function handleSearchText(e) {
    if(e.which == 13) { // 13 is the code for "Enter"
        refreshText(e.target.value);
    } else {
        getSuggestions(e.target.value);
    }
}

$(function () {
    // No princípio coloca os handlers para os eventos
    $("#searchtext").keyup(handleSearchText);
    $("#searchbigtext").keyup(handleSearchText);
    $("#searchbigbutton").click(refreshBig);
});