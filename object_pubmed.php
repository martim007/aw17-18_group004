<?php
class Pubmed{

    // database connection and table name
    private $conn;
    private $table_name = "PubMed";

    // object properties
    public $Abstract;
    public $Authors;
    public $A_ID;
    public $Date;
    public $TermCount;
    public $Title;


    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read Titles abstracts
    function read(){

        // select all query
        $query = " SELECT
        p.Title, p.A_ID
    FROM
        " . $this->table_name . " p ORDER BY p.A_ID ASC
        ";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }

        function read_one($A_ID){
       
    // // query to read single disease
        $query = "SELECT p.A_ID, p.Title, p.Authors, p.Date, p.Abstract 
            FROM " . $this->table_name . " p WHERE p.A_ID = " . $A_ID . " ";

    // // prepare query statement
        $stmt = $this->conn->prepare( $query );

    // // execute query
        $stmt->execute();
        return $stmt;
    }

    function read_related_dis($A_ID){
    // // query to read single disease
        $query = "SELECT DISTINCT(d.D_ID), sad.SA_ID, d.Name
        FROM Diseases d, Articles a, SortAlphabeticalDis sad
        WHERE a.A_ID = " . $A_ID . " AND d.D_ID = a.D_ID and d.D_ID = sad.D_ID;";

    // // prepare query statement
        $stmt = $this->conn->prepare( $query );
    // // execute query
        $stmt->execute();
        return $stmt;
    }
}
?>