<?php
class Flickr{

    // database connection and table name
    private $conn;
    private $table_name = "Flickr";

    // object properties
    public $Date;
    public $F_ID;
    public $F_URL;
    public $Tags;
    public $TermCount;


    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

       // read pictures
       function read(){

        // select all query
        $query = "SELECT
                    f.F_ID, f.F_URL
                FROM
                    " . $this->table_name . " f
                    ORDER BY f.F_ID
                    ";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }


    function read_one($F_ID){

        // // query to read single flickr
            $query = "SELECT
                f.F_ID, f.Date, f.F_URL, f.Tags, f.TermCount
                FROM
                " . $this->table_name . " f
                WHERE
                f.F_ID = " . $F_ID . "
                ";

        // // prepare query statement
            $stmt = $this->conn->prepare( $query );

        // // execute query

            $stmt->execute();

            return $stmt;


        }

}
?>