<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once 'database.php';
include_once 'object_tweet.php';
include_once 'SimpleRest.php';


class ReadOne extends SimpleRest {

  public function __construct(){
  }

	function readOne($tid)  {
	// get database connection
	$database = new Database();
	$db = $database->getConnection();


	// prepare tweets object
	$tweet = new Tweet($db);


	// read the details of tweets to be edited
	$stmt = $tweet->read_one($tid);
	// echo($id);
	// print_r($stmt);
	if (empty($stmt)) {
          $statusCode= 404;
          $stmt = array('error' => 'No id found!');
      } else {
			$statusCode = 200;
      }
      $requestContentType = $_SERVER['HTTP_ACCEPT'];
  		$this ->setHttpHeaders($requestContentType, $statusCode);

  		if(strpos($requestContentType,'application/json') !== false){
  			$response = $this->encodeJson($stmt);
  			echo $response;
  		} else if(strpos($requestContentType,'text/html') !== false){
        $response = $this->encodeHtml($stmt);
  			echo $response;
  		} else if(strpos($requestContentType,'application/xml') !== false){
      	$response = $this->encodeXml($stmt);
  			echo $response;
  		}
    }

  	public function encodeHtml($responseData) {
		if (!empty($responseData)) {
	  $htmlResponse = "<table border='1'>";

	  $htmlResponse .= "<thead><tr><th>ID</th><th>URL</th><th>Date</th><th>Tweet</th><th>Term Count</th></tr></thead>";
  		foreach($responseData as $key=>$value) {
			//var_dump($value);
  		    if (!empty($value)) {
				  $htmlResponse .= "<tr><td>". ($value["T_ID"]). "</td><td>". strtolower($value["T_URL"]). "</td><td>".
				  ($value["PostDate"]). "</td><td>".($value["Tweet"]) . "</td><td>".
				  ucwords($value["TermCount"]) ."</td></tr>";
          }
  		}
  		$htmlResponse .= "</table>";
		  return "<html>".$htmlResponse."</html>";
	}
  	}

  	public function encodeJson($responseData) {

  		// article array
  		$tweet_arr=array();
  		foreach($responseData as $key=>$value) {
        if($value["PostDate"]=="0001-01-01"){
          $value["PostDate"]="Unknown";
        }
  			// var_dump($key, $value["Name"]);
        $jsonResponse = array("tweet_id" => ($value["T_ID"]), "src" => strtolower($value["T_URL"]), "date" => ($value["PostDate"]),
              "text" => ($value["Tweet"]), "term_count" => ($value["TermCount"]));
        $jsonResponse = json_encode($jsonResponse, JSON_UNESCAPED_UNICODE);
  		}
  		return $jsonResponse;
    }

  	public function encodeXml($responseData) {
      //var_dump( $responseData);

  		// creating object of SimpleXMLElement
  		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><tweet></tweet>');
      //echo $xml;
  		foreach($responseData as $key=>$value) {
        if($value["PostDate"]=="0001-01-01"){
          $value["PostDate"]="Unknown";
        }
  			$xml->addChild("ID", ($value["T_ID"]));
        $xml->addChild("URL", strtolower($value["T_URL"]));
        $xml->addChild("Date", ($value["PostDate"]));
        $xml->addChild("Tweet", ($value["Tweet"]));
        $xml->addChild("Term Count", ($value["TermCount"]));
      }
  		return $xml->asXML();
 	}
}
?>