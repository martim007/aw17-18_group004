<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once 'database.php';
include_once 'object_pubmed.php';
include_once 'SimpleRest.php';


class Read extends SimpleRest {

    function readAll() {

      // instantiate database and pubmed object
      $database = new Database();
      $db = $database->getConnection();

      // initialize object
      $pubmed = new Pubmed($db);
      // query pubmed
	  $stmt = $pubmed->read();


      //$num = $stmt->rowCount();
      if (empty($stmt)) {
          $statusCode= 404;
          $stmt = array('error' => 'No articles found!');
      } else {
			$statusCode = 200;


      }
      $requestContentType = $_SERVER['HTTP_ACCEPT'];
  		$this ->setHttpHeaders($requestContentType, $statusCode);

  		if(strpos($requestContentType,'application/json') !== false){
  			$response = $this->encodeJson($stmt);
  			echo $response;
  		} else if(strpos($requestContentType,'text/html') !== false){
        $response = $this->encodeHtml($stmt);
  			echo $response;
  		} else if(strpos($requestContentType,'application/xml') !== false){
      	$response = $this->encodeXml($stmt);
  			echo $response;
  		}
    	}

  	public function encodeHtml($responseData) {

      $htmlResponse = "<table border='1'>";
  		foreach($responseData as $key=>$value) {
			// var_dump($value);
  		    if (!empty($value)) {
      		    $htmlResponse .= "<tr><td>". ($value["A_ID"]). "</td><td>". ucwords($value["Title"]). "</td></tr>";
          }
  		}
  		$htmlResponse .= "</table>";
  		return "<html>".$htmlResponse."</html>";
  	}

  	public function encodeJson($responseData) {

		// article array
		$pubmed_arr=array();
		foreach($responseData as $key=>$value) {
			// var_dump($key, $value["Title"]);
			$jsonResponse = array("ID" => ($value["A_ID"]), "Pubmed articles" => ucwords($value["Title"]));
			array_push($pubmed_arr, $jsonResponse);
		  }
		// var_dump( $pubmed_arr);
		$jsonResponse = json_encode($pubmed_arr, JSON_UNESCAPED_UNICODE);
		// echo $jsonResponse;
  		return $jsonResponse;
  	}

  	public function encodeXml($responseData) {
  		// creating object of SimpleXMLElement
  		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><pubmed></pubmed>');
  		foreach($responseData as $key=>$value) {
        $xml1 = $xml->addChild("Nr", ($key+1));
  			$xml1->addChild("ID",($value["A_ID"]));
        $xml1->addChild("Title",strtolower($value["Title"]));
  		}
  		return $xml->asXML();
  	}
}
?>